package com.novikdm.view;

import com.novikdm.controller.Controller;
import com.novikdm.controller.ViewControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleMenu {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private Scanner inputLine = new Scanner(System.in);
  private static Logger print = LogManager.getLogger(ConsoleMenu.class);

  public ConsoleMenu() {
    controller = new ViewControllerImpl();
    methodsMenu = new LinkedHashMap<>();

    menu = new LinkedHashMap<>();
    menu.put("1", "  1 - Test1");
    menu.put("2", "  2 - Test2");
    menu.put("3", "  3 - Test3");

    menu.put("3", "  0 - Exit");

    methodsMenu.put("1", this::test1);
    methodsMenu.put("2", this::test2);
    methodsMenu.put("3", this::test3);
  }

  private void test1() {
    print.trace("test2");
  }

  private void test2() {
    print.trace("test2");
  }

  private void test3() {
    print.trace("test3");
  }

  private void outputMenu() {
    System.out.println("MENU:");
    for (String str : menu.values()) {
      print.trace(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      print.trace("Please, select menu point.");
      keyMenu = inputLine.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("0"));
  }
}
